---
title: "OtolithOverview"
author: "Stephanie Stuart"
date: "11 July 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Collegues' Requests

## How they would like us to reconstruct the files
* Reconstruct spatial variation in elemental analysis of otoliths from transects
*  Correct (point) readings for:
    * Background - each otolith reading includes blank transects as the first and last transect. There is also blank space at the beginning and end of each transect. The background readings are different for the beginning and the end. This must be subtracted from each point measurement, while accounting for drift. Sandy is not sure how this is calculated, but thinks details can be found in the Excel macro she has provided.
    * Elemental standards – standards are run at the beginning and end of each day, and at various times through the day. The number of samples and transects measured between standards varies.  Math should come from the Excel macro, standard values are in the excel files. We must correct for
          * any difference between the measurement of the standard at the known value
          * any drift between standard measurements
    * Measurements which are 'below detection limits'
    * Correct for 'spurious values' -- current limit is set at greater than 3% difference in standard deviation. The Excel macro apparently involves some kind of smoothing. Presumably this is done once point measurements have been corrected...
    
## Data they would like to extract from our results
*	Heatmap – set threshold values
*	Ability to do new randomly chosen transects/counting lines
*	For a given counting line, graphs and output data across the otolith (based on point measurements)
*	Set a manual window along a given counting line, and get averages for a specific range within the otolith
*	Jeff’s suggestion – pick your own points and then get coordinates and results (library(locator))
*	ratios of elements, mostly wrt to Calcium -- these could be prepared by dividing the heatmap data


# Programming Outline
1.	Read in the otolith files transect
2.  Reconstruct the transects into a whole otolith (or at least a stripey otolith)

#	Needed from Colleagues
##Date-specific data
1.	Order of samples on day, including number of standards
2.	Speed
3.	Box size
4.	Spot size (width of transect)
5.	Frequency of laser (intensity) -- this sounds more like it is another thing to correct for?
6.	y coordinate of transects -- in fact they do not know this. However, given the number of transects (which should be equally distributed within bounding box – we may be able to calculate the separation between transects.) Otherwise, there may be some kind of information about this traceable through the Excel macro and/or the additional data files provided by the mass spec.
