[Header Info]
045.D	D:\DATA\SANDY\20140620\158.b\	'	ICP-MS	C:\ICPCHEM\1\CALIB\	SD158.C	---		---	---	Point to Point
Jun 21 2014  09:03 pm	SD158.M	'	'Sample	C:\ICPCHEM\1\METHODS\	SD158.M
---	45	1.000	Undiluted	1.000	---	
'
'Sample calibration curve	Oct 11 2007  05:42 pm	External Calibration Method	1/(SD*SD)
																			
Timechart	--------				OFF	--------	QC
0	---	---	---	---	---	---	---	---	---	---	---	---	---	---	---
--------	0	0	---	---		Pass	---	---			Off	---	---	Off	---	---	Off	---	---	Off	---	---	---	---	---	---	---	---
--------	--------	OFF	Normal Mode	ug/l
0		0	        0.00		0.00E+000	0.000E+000	
0		0	        0.00		0.00E+000	0.000E+000	
0		0	        0.00		0.00E+000	0.000E+000	
0		0	        0.00		0.00E+000	0.000E+000	





---	---	---	---	---	---	---	---	---	---
---	---	---	---	---	---	---	---	---	---
---	---	---	---	---	---	---	---	---	---
















[Semi-Quant Ions]
																																																																																													
																																																																																													
  3	        Li	  7	--------	mg/l	No Data	--------	P	0.0	'lithium	--------	 73050.000																																																																																	
  4	        Be	  9	--------	mg/l	No Data	--------	P	0.0	'beryllium	--------	 17670.000																																																																																	
  5	         B	 11	--------	mg/l	No Data	--------	P	0.0	'boron	--------	 15390.000																																																																																	
  6	         C	 12	--------	mg/l	No Data	--------	P	0.0	'carbon	--------	  2873.000																																																																																	
  7	         N	 14	--------	mg/l	No Data	--------	P	0.0	'nitrogen	--------	    26.300																																																																																	
																																																																																													
																																																																																													
																																																																																													
 11	        Na	 23	--------	mg/l	No Data	--------	P	0.0	'sodium	--------	 97720.000																																																																																	
 12	        Mg	 24	         1.0	ng/l	--------	       66.67	M	0.0	'magnesium	0.78	 63830.000																																																																																	
 13	        Al	 27	--------	mg/l	No Data	--------	P	0.0	'aluminium	--------	 81420.000																																																																																	
 14	        Si	 29	--------	mg/l	No Data	--------	P	0.0	'silicon	--------	  1939.000																																																																																	
 15	         P	 31	--------	mg/l	No Data	--------	P	0.0	'phosphorus	--------	  5416.000																																																																																	
 16	         S	 34	--------	mg/l	No Data	--------	P	0.0	'sulfur	--------	   366.000																																																																																	
 17	        Cl	 35	--------	mg/l	No Data	--------	P	0.0	'chlorine	--------	   238.100																																																																																	
																																																																																													
 19	         K	 39	--------	mg/l	No Data	--------	P	0.0	'potassium	--------	145200.000																																																																																	
 20	        Ca	 43	--------	mg/l	No Data	--------	P	0.0	'calcium	--------	   397.400																																																																																	
 21	        Sc	 45	--------	mg/l	No Data	--------	P	0.0	'scandium	--------	122700.000																																																																																	
 22	        Ti	 47	--------	mg/l	No Data	--------	P	0.0	'titanium	--------	  7906.000																																																																																	
 23	         V	 51	0.46	ng/l	--------	       50.00	P	0.0	'vanadium	0.46	107300.000																																																																																	
 24	        Cr	 53	--------	mg/l	No Data	--------	P	0.0	'chromium	--------	 11130.000																																																																																	
 25	        Mn	 55	          17	ng/l	--------	     2250.17	P	0.0	'manganese	0.39	125400.000																																																																																	
 26	        Fe	 57	--------	mg/l	No Data	--------	P	0.0	'iron	--------	  2772.000																																																																																	
 27	        Co	 59	--------	mg/l	No Data	--------	P	0.0	'cobalt	--------	104800.000																																																																																	
 28	        Ni	 60	--------	mg/l	No Data	--------	P	0.0	'nickel	--------	 21940.000																																																																																	
 29	        Cu	 63	--------	mg/l	No Data	--------	P	0.0	'copper	--------	 52310.000																																																																																	
 30	        Zn	 66	--------	mg/l	No Data	--------	P	0.0	'zinc	--------	 15280.000																																																																																	
 31	        Ga	 69	--------	mg/l	No Data	--------	P	0.0	'gallium	--------	 80600.000																																																																																	
 32	        Ge	 72	--------	mg/l	No Data	--------	P	0.0	'germanium	--------	 22650.000																																																																																	
 33	        As	 75	--------	mg/l	No Data	--------	P	0.0	'arsenic	--------	 12390.000																																																																																	
 34	        Se	 82	--------	mg/l	No Data	--------	P	0.0	'selenium	--------	  1543.000																																																																																	
 35	        Br	 79	--------	mg/l	No Data	--------	P	0.0	'bromine	--------	  1814.000																																																																																	
																																																																																													
 37	        Rb	 85	--------	mg/l	Not Detected	       33.33	P	0.0	'rubidium	0.00000051	 97280.000																																																																																	
 38	        Sr	 88	0.52	ng/l	--------	       66.67	M	0.0	'strontium	0.39	128000.000																																																																																	
 39	         Y	 89	--------	mg/l	No Data	--------	P	0.0	'yttrium	--------	162500.000																																																																																	
 40	        Zr	 90	--------	mg/l	No Data	--------	P	0.0	'zirconium	--------	 79800.000																																																																																	
 41	        Nb	 93	--------	mg/l	No Data	--------	P	0.0	'niobium	--------	135800.000																																																																																	
 42	        Mo	 95	--------	mg/l	No Data	--------	P	0.0	'molybdenum	--------	 22810.000																																																																																	
																																																																																													
 44	        Ru	101	--------	mg/l	No Data	--------	P	0.0	'ruthenium	--------	 25040.000																																																																																	
 45	        Rh	103	--------	mg/l	No Data	--------	P	0.0	'rhodium	--------	139200.000																																																																																	
 46	        Pd	105	--------	mg/l	No Data	--------	P	0.0	'palladium	--------	 29880.000																																																																																	
 47	        Ag	107	--------	mg/l	No Data	--------	P	0.0	'silver	--------	 58990.000																																																																																	
 48	        Cd	111	--------	mg/l	No Data	--------	P	0.0	'cadmium	--------	 13220.000																																																																																	
 49	        In	115	--------	mg/l	No Data	--------	P	0.0	'indium	--------	140300.000																																																																																	
 50	        Sn	118	--------	mg/l	No Data	--------	P	0.0	'tin	--------	 34500.000																																																																																	
 51	        Sb	121	--------	mg/l	No Data	--------	P	0.0	'antimony	--------	 40680.000																																																																																	
 52	        Te	125	--------	mg/l	No Data	--------	P	0.0	'tellurium	--------	  2319.000																																																																																	
 53	         I	127	--------	mg/l	No Data	--------	P	0.0	'iodine	--------	 21510.000																																																																																	
																																																																																													
 55	        Cs	133	--------	mg/l	No Data	--------	P	0.0	'cesium	--------	133400.000																																																																																	
 56	        Ba	137	--------	mg/l	No Data	--------	P	0.0	'barium	--------	 16510.000																																																																																	
 57	        La	139	--------	mg/l	No Data	--------	P	0.0	'lanthanum	--------	153600.000																																																																																	
 58	        Ce	140	0.34	ng/l	--------	       50.00	P	0.0	'cerium	0.34	145800.000																																																																																	
 59	        Pr	141	--------	mg/l	No Data	--------	P	0.0	'praseodymium	--------	176100.000																																																																																	
 60	        Nd	146	--------	mg/l	No Data	--------	P	0.0	'neodymium	--------	 29690.000																																																																																	
																																																																																													
 62	        Sm	147	--------	mg/l	No Data	--------	P	0.0	'samarium	--------	 24900.000																																																																																	
 63	        Eu	153	--------	mg/l	No Data	--------	P	0.0	'europium	--------	 87900.000																																																																																	
 64	        Gd	157	--------	mg/l	No Data	--------	P	0.0	'gadolinium	--------	 27480.000																																																																																	
 65	        Tb	159	--------	mg/l	No Data	--------	P	0.0	'terbium	--------	164000.000																																																																																	
 66	        Dy	163	--------	mg/l	No Data	--------	P	0.0	'dysprosium	--------	 39590.000																																																																																	
 67	        Ho	165	--------	mg/l	No Data	--------	P	0.0	'holmium	--------	155800.000																																																																																	
 68	        Er	166	--------	mg/l	No Data	--------	P	0.0	'erbium	--------	 51740.000																																																																																	
 69	        Tm	169	--------	mg/l	No Data	--------	P	0.0	'thulium	--------	156200.000																																																																																	
 70	        Yb	172	--------	mg/l	No Data	--------	P	0.0	'ytterbium	--------	 34360.000																																																																																	
 71	        Lu	175	--------	mg/l	No Data	--------	P	0.0	'lutetium	--------	147300.000																																																																																	
 72	        Hf	178	--------	mg/l	No Data	--------	P	0.0	'hafnium	--------	 41160.000																																																																																	
 73	        Ta	181	--------	mg/l	No Data	--------	P	0.0	'tantalum	--------	131500.000																																																																																	
 74	         W	182	--------	mg/l	No Data	--------	P	0.0	'tungsten	--------	 33480.000																																																																																	
 75	        Re	185	--------	mg/l	No Data	--------	P	0.0	'rhenium	--------	 46970.000																																																																																	
 76	        Os	189	--------	mg/l	No Data	--------	P	0.0	'osmium	--------	 22440.000																																																																																	
 77	        Ir	193	--------	mg/l	No Data	--------	P	0.0	'iridium	--------	 62830.000																																																																																	
 78	        Pt	195	--------	mg/l	No Data	--------	P	0.0	'platinum	--------	 23650.000																																																																																	
 79	        Au	197	--------	mg/l	No Data	--------	P	0.0	'gold	--------	 36560.000																																																																																	
 80	        Hg	202	--------	mg/l	No Data	--------	P	0.0	'mercury	--------	  9979.000																																																																																	
 81	        Tl	205	--------	mg/l	No Data	--------	P	0.0	'thallium	--------	 76120.000																																																																																	
 82	        Pb	208	--------	mg/l	Not Detected	       25.00	M	0.0	'lead	0.00000093	 53290.000																																																																																	
 83	        Bi	209	--------	mg/l	No Data	--------	P	0.0	'bismuth	--------	 84110.000																																																																																	
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
 90	        Th	232	--------	mg/l	No Data	--------	P	0.0	'thorium	--------	 98830.000																																																																																	
																																																																																													
 92	         U	238	--------	mg/l	No Data	--------	P	0.0	'uranium	--------	 97100.000																																																																																	
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													






