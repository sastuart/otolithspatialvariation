# Otolith Spatial Variation #

The purpose of this project is to reconstruct spatial variation in otoliths (fish ear bones) from transects made by a laser ablation/mass spec combination.

* This repository is owned by [Stephanie Stuart](sastuart@gmail.com)
* The project is coordinated by [Jeff Powell](http://www.uws.edu.au/hie/people/researchers/assoc_prof_jeff_powell)
* The analysis related to research being done by [Sandra Diamond-Tissue](http://www.uws.edu.au/staff_profiles/uws_profiles/doctor_sandra_diamond-tissue) and her students