# function to read data from all transects within a sample
# all data from each sample is stored in a single folder -- this directory is identified by the first argument in the function
# data from each transect is stored in a separate subfolder within the main folder
# elemental data are found in the .csv file within each subfolder

read.sample <- function(dir.sample=NULL) {
  # flag to ensure sample folder is specified
  if(is.null(dir.sample)) stop('specify path to sample folder')
  
  # specify path to subfolders containing transect data
  dir.transect <- paste(dir.sample, dir(dir.sample), sep='/')
  dir.transect <- dir.transect[grep('.D$', dir.transect)]
  
  # create a list in which to store data and loop through the subfolders reading in the data from the .csv file, add column containing transect number ('y' coordinate)
  data <- vector('list', length=length(dir.transect))
  names(data) <- dir.transect
  for(i in 1:length(dir.transect)) {
    data[[i]] <- read.csv(paste(dir.transect[i], grep('.csv$', dir(dir.transect[i]), value=T), sep='/'), skip=2)
    data[[i]]$y <- dir.transect[i]
  }
  
  # combine into single data.frame, rename first column as 'x', move 'y' to second column
  data <- do.call(rbind, data)
  names(data)[1] <- 'x'
  data <- data[, c(1, ncol(data), 2:(ncol(data) - 1))]
  
  # convert text in column 'y' to numeric, indicating transect number
  data[['y']] <- as.numeric(gsub('.D$', '', gsub(paste(dir.sample, '/', sep=''), '', data[['y']])))
  
  # print output
  data
}